import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import org.postgresql.util.PSQLException;

public class Main {
    // JDBC driver name and database URL
    static final String DB_URL = "jdbc:postgresql://localhost:5432/company";

    //  Database credentials
    static final String USER = "picroc";
    static final String PASS = "12345";

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        try{
            Class.forName("org.postgresql.Driver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn.setAutoCommit(false);


            System.out.println("Creating table...");
            stmt = conn.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);

            stmt.execute("create table if not exists salary(id integer primary key unique, employee_id integer unique, amount integer)");
            stmt.execute("truncate salary");
            conn.commit();

            LinkedList<Integer> ids = new LinkedList<Integer>();
            LinkedList<Integer> employee_ids = new LinkedList<Integer>();
            LinkedList<Integer> amounts = new LinkedList<Integer>();

            ids.add(1);
            ids.add(2);

            employee_ids.add(5);
            employee_ids.add(10);

            amounts.add(100);
            amounts.add(100);

            System.out.println("Inserting init rows....");
            addSalaries(conn, ids, employee_ids, amounts);

            String sql = "SELECT id, employee_id, amount FROM salary";
            ResultSet rs = stmt.executeQuery(sql);
            System.out.println("List result set for reference....");
            printRs(rs);
            rs.close();

            ids.clear();
            employee_ids.clear();
            amounts.clear();

            ids.add(1);
            employee_ids.add(15);
            amounts.add(100);

            System.out.println("Inserting with wrong id....");
            addSalaries(conn, ids, employee_ids, amounts);

            ids.clear();
            employee_ids.clear();
            amounts.clear();

            ids.add(3);
            employee_ids.add(5);
            amounts.add(100);

            System.out.println("Inserting with wrong employee_id....");
            addSalaries(conn, ids, employee_ids, amounts);

            stmt.close();
            conn.close();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }
        System.out.println("Goodbye!");
    }

    public static void printRs(ResultSet rs) throws SQLException{
        //Ensure we start with first row
        rs.beforeFirst();
        while(rs.next()){
            //Retrieve by column name
            int id  = rs.getInt("id");
            int employee_id = rs.getInt("employee_id");
            int amount = rs.getInt("amount");

            //Display values
            System.out.print("ID: " + id);
            System.out.print(", Employee ID: " + employee_id);
            System.out.println(", Amount: " + amount);
        }
        System.out.println();
    }

    public static void addSalaries(Connection conn,
                                   LinkedList<Integer> ids,
                                   LinkedList<Integer> employee_ids,
                                   LinkedList<Integer> amount) throws SQLException {
        try {
            Statement stmt = conn.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);

            for (int i = 0, n = ids.size(); i < n; i++) {
                String SQL = "INSERT INTO Salary " +
                        "VALUES (" + ids.get(i) + "," +
                        employee_ids.get(i) + "," +
                        amount.get(i) + ")";

                stmt.executeUpdate(SQL);
            }
            conn.commit();
            stmt.close();
        } catch (PSQLException throwables) {
            throwables.printStackTrace();
            System.out.println("Rolling back data here....");
            try{
                if(conn!=null)
                    conn.rollback();
            }catch(SQLException se2){
                se2.printStackTrace();
            }
        }
    }
}
